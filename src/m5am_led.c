#include <driver/rmt.h>
#include <led_strip.h>
#include <string.h>

#include <m5am_led.h>

#define M5AM_LED_PIN 27

#define TO_PIX_FROM_8BPP(val) (struct led_color_rgb) \
	{.r = 0xE & (val)>>4, .g = 0xE & ((val)>>1), .b = 0xC & ((val)<<2)}

#define TO_PIX_FROM_12BPP(val) (struct led_color_rgb) \
	{.r = 0xF & ((val)>>8), .g = 0xF & ((val)>>4), .b = 0xF & (val)}

// LUT with pixel order depending on rotation
static const uint8_t rot_lut[__LED_ROTATION_MAX][M5AM_LEDS] = {
	{
		 0,  1,  2,  3,  4,
		 5,  6,  7,  8,  9,
		10, 11, 12, 13, 14,
		15, 16, 17, 18, 19,
		20, 21, 22, 23, 24
	},
	{
		 4,  9, 14, 19, 24,
		 3,  8, 13, 18, 23,
		 2,  7, 12, 17, 22,
		 1,  6, 11, 16, 21,
		 0,  5, 10, 15, 20
	},
	{
		24, 23, 22, 21, 20,
		19, 18, 17, 16, 15,
		14, 13, 12, 11, 10,
		 9,  8,  7,  6,  5,
		 4,  3,  2,  1,  0
	},
	{
		20, 15, 10,  5,  0,
		21, 16, 11,  6,  1,
		22, 17, 12,  7,  2,
		23, 18, 13,  8,  3,
		24, 19, 14,  9,  4
	}
};

static led_strip_t *led = NULL;
static const struct led_color_rgb *palette = NULL;
static enum led_rotation rot = LED_ROTATION_0;

void led_palette_clear(void)
{
	palette = NULL;
}

void led_palette_set(const struct led_color_rgb *colors)
{
	palette = colors;
}

void led_clear(struct led_color_rgb color)
{
	for (uint32_t i = 0; i < M5AM_LEDS; i++) {
		led->set_pixel(led, rot_lut[rot][i], color.r, color.g, color.b);
	}
	led->refresh(led, 100);
}

void led_set(const struct led_color_rgb *leds)
{
	for (uint32_t i = 0; i < M5AM_LEDS; i++) {
		led->set_pixel(led, rot_lut[rot][i], leds[i].r, leds[i].g,
				leds[i].b);
	}
	led->refresh(led, 100);
}

static void led_draw_indexed(const uint8_t *pixels)
{
	for (uint32_t i = 0; i < M5AM_LEDS; i++) {
		const struct led_color_rgb pixel = palette[pixels[i]];
		led->set_pixel(led, rot_lut[rot][i], pixel.r, pixel.g, pixel.b);
	}
	led->refresh(led, 100);
}

static void led_draw_8bpp(const uint8_t *pixels)
{
	for (uint32_t i = 0; i < M5AM_LEDS; i++) {
		const struct led_color_rgb pixel = TO_PIX_FROM_8BPP(pixels[i]);
		led->set_pixel(led, rot_lut[rot][i], pixel.r, pixel.g, pixel.b);
	}
	led->refresh(led, 100);
}

void led_draw_12bpp(const uint16_t *pixels)
{
	for (uint32_t i = 0; i < M5AM_LEDS; i++) {
		const struct led_color_rgb pixel = TO_PIX_FROM_12BPP(pixels[i]);
		led->set_pixel(led, rot_lut[rot][i], pixel.r, pixel.g, pixel.b);
	}
	led->refresh(led, 100);
}

void led_draw(const uint8_t *pixels)
{
	if (palette) {
		led_draw_indexed(pixels);
	} else {
		led_draw_8bpp(pixels);
	}
}

bool led_init(void)
{
	bool err = false;
	rmt_config_t rmt_cfg = RMT_DEFAULT_CONFIG_TX(M5AM_LED_PIN, RMT_CHANNEL_0);
	rmt_cfg.clk_div = 2;

	ESP_ERROR_CHECK(rmt_config(&rmt_cfg));
	ESP_ERROR_CHECK(rmt_driver_install(rmt_cfg.channel, 0, 0));

	led_strip_config_t led_cfg = {
		.max_leds = M5AM_LEDS,
		.dev = (led_strip_dev_t)rmt_cfg.channel
	};

	led = led_strip_new_rmt_ws2812(&led_cfg);

	if (!led) {
		err = true;
	}

	return err;
}

void led_rotation_set(enum led_rotation rotation)
{
	rot = rotation;
}

enum led_rotation led_rotation_get(void)
{
	return rot;
}

void led_draw_pixel(uint_fast8_t x, uint_fast8_t y, struct led_color_rgb color)
{
	int i = x + 5 * y;
	led->set_pixel(led, rot_lut[rot][i], color.r, color.g, color.b);
}

void led_draw_pixel_lin(uint_fast8_t pos, struct led_color_rgb color)
{
	led->set_pixel(led, rot_lut[rot][pos], color.r, color.g, color.b);
}

void led_update(void)
{
	led->refresh(led, 100);
}

