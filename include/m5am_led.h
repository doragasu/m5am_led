/************************************************************************//**
 * Simple RGB LED module for M5Atom Lite
 *
 * There are 4 different ways of drawing pixels:
 * 1. Call led_set() to write all the pixels in 24-bit RGB values,
 *    8-bits per component.
 * 2. Call led_draw() without previously setting a palette, to write all the
 *    pixels in RGB332 format.
 * 3. Set a custom palette (up to 256 color) with led_palette_set(), then
 *    call led_draw() to write all the pixels, indexing in the palette.
 * 4. Write all the pixels in RGB444 format by calling led_draw_12bpp().
 *
 * You can mix all this modes as you wish.
 *
 * \warning For M5Atom boards, setting a value greater than 20, can cause
 * overheating, damaging the device. This can only be done when using the
 * functions that allow specifying 24-bit values: led_set() and led_clear().
 * \note When using 8bpp mode, maximum LED values are 14 for r and g
 * components, and 12 for the blue component. When using 12bpp mode, maximum
 * values are 15 for each component. When using custom palette or the raw
 * led_set() function, maximum value is 255, but more than 20 should not be
 * used.
 ****************************************************************************/
#ifndef _LED_H_
#define _LED_H_

#include <stdint.h>
#include <stdbool.h>

// Number of total leds
#define M5AM_LEDS 25

// Pixel composed of 3-bit red, 3-bit green and 2-bit blue
#define LED_8BPP_PIX(r, g, b) (((uint8_t)(r)<<5) | ((uint8_t)(g)<<2) | \
		(uint8_t)(b))
// Pixel composed of 4-bit each r, g, b
#define LED_12BPP_PIX(r, g, b) (((uint16_t)(r)<<8) | ((uint16_t)(g)<<4) | \
		(uint16_t)(b))

struct led_color_rgb {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

enum led_rotation {
	LED_ROTATION_0 = 0,
	LED_ROTATION_90,
	LED_ROTATION_180,
	LED_ROTATION_270,
	__LED_ROTATION_MAX
};

bool led_init(void);
void led_rotation_set(enum led_rotation rotation);
enum led_rotation led_rotation_get(void);
// NOTE: This function does not copy the palette, just takes its reference.
// Thus the palette data must be valid for the lifetime of the module.
void led_palette_set(const struct led_color_rgb *colors);
void led_palette_clear(void);
// Clear the screen, setting all the pixels to the specified RGB value
void led_clear(struct led_color_rgb color);
void led_set(const struct led_color_rgb *leds);
void led_draw(const uint8_t *pixels);
void led_draw_12bpp(const uint16_t *pixels);

// led_draw_pixel() and led_draw_pixel_lin() do not update the leds when
// finished. Call led_update() afterwards for the changes to take effect
void led_draw_pixel(uint_fast8_t x, uint_fast8_t y, struct led_color_rgb color);
void led_draw_pixel_lin(uint_fast8_t pos, struct led_color_rgb color);
void led_update(void);

#endif /*_LED_H_*/

