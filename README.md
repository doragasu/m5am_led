# Introduction

This is a simple driver for the 5x5 LED matrix embedded in the M5Atom Matrix boards. It is intended to be used with the `esp-idf` SDK, and has no Arduino dependencies. The driver aims to be flexible and simple. You can write pixels to the matrix in 24-bit RGB format, 8-bit RGB332 format, 12-bit RGB444 format, and with 256 color indexed palettes. You can also set the screen rotation in 90 degree steps. That's all, no more fancy functions to draw points, lines or whatever, just push the pixels to the "screen" and get them displayed.

![m5atom](img/m5atom.jpg)

# Usage

As with any esp-idf component, clone this project in the `components` subdirectory of your project. Then you should be able to `#include <m5am_led.h>` and start using the module right away. Using the module is easy: just call `led_init()` once, and optionally set the rotation with `led_rotation_set()` function. Then, to write pixels you have 4 different ways you can combine as you wish:

 1. Call `led_set()` to write all the pixels in 24-bit RGB values, 8-bits per component.
 2. Call `led_draw()` without previously setting a palette, to write all the pixels in 8-bit RGB332 format.
 3. Set a custom palette (up to 256 color) with `led_palette_set()`, then call `led_draw()` to write all the pixels, indexing the palette.
 4. Write all the pixels in 12-bit RGB444 format by calling `led_draw_12bpp()`.

# Warning

The maximum value that can be pushed to each RGB component in the LED matrix depends on the mode used to write:

| Mode                    | RED | GREEN | BLUE |
| ----------------------- | --- | ----- | ---- |
| led\_set()              | 255 |  255  |  255 |
| led\_draw() w/o palette |  14 |   14  |   12 |
| led\_draw() w/ palette  | 255 |  255  |  255 |
| led\draw\_12bpp()       |  15 |   15  |   15 |

According to the M5Atom developers, you should not set values greater than 20 for extended time periods to avoid possible damage due to overheating. For the `led_draw()` without setting the indexed color palette, and for the `led_draw_12bpp()` modes, you are safe, maximum value never reaches 20, and with the values you can see on the table, the LEDs are pretty bright, maybe you will not need to go beyond. But if you use `led_set()` or `led_draw()` function with a custom indexed palette, you will be able to go up to 255. This could cause damage to the device, so make sure you know what you are doing when using these modes. In other words, **this module will not stop you from damaging your device** if you set the LEDs way too bright.

# License

This program is provided with NO WARRANTY, under the [Mozilla Public License (MPL)](https://www.mozilla.org/en-US/MPL/2.0/). I am not responsible in any way if the code does not work, or causes any harm. Make sure you read the warning above in this regard.

